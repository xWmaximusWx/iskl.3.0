﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using NavigationDrawerPopUpMenu2;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ИСКО
{
    /// <summary>
    /// Логика взаимодействия для Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public MongoClient client;
        public IMongoDatabase db;
        public IMongoCollection<User> collection;
        static bool enable; 
        public static User user { get; set; }
        public Login()
        {
            InitializeComponent();
            enable = false;

            client = new MongoClient("mongodb+srv://user:zOv1pGKMz0DJ4LfE@cluster0-at0y4.azure.mongodb.net/<dbname>?retryWrites=true&w=majority");
            db = client.GetDatabase("SystemCorporateEdcation");
            collection = db.GetCollection<User>("Users");

            //user = new User
            //{
            //    FName = "Александр",
            //    MName = "Петрович",
            //    LName = "Городовиков",
            //    PhoneNumber = "+74956997369",
            //    Email = "alex@mail.ru",
            //    Status = false,
            //    Login = "alex",
            //    Password = "12345",
            //    Imgage = "https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/User_icon-cp.svg/1200px-User_icon-cp.svg.png",
            //    Work = "ЮФУ",
            //    Group = "КТбо4-5"
            //};

            //collection.InsertOne(user);
        }

        private void LoadMainWindow(object sender, RoutedEventArgs e)
        {
            LoginUser(logInput.Text, pasInput.Password);
            //LoginUser("vlada", "12345");
            if(enable)
            {
                if(user.Status)
                {
                    MainWindowAdmin mainWindowAdmin = new MainWindowAdmin(user);
                    mainWindowAdmin.Show();
                    this.Visibility = Visibility.Hidden;
                } else
                {
                    MainWindowUser mainWindowUser = new MainWindowUser(user);
                    mainWindowUser.Show();
                    this.Visibility = Visibility.Hidden;
                }
            }
        }


        private void LoginUser(string log, string pass)
        {
            List<User> users = collection.Find(user => user.Login == log && user.Password == pass).ToList(); 
            if (users.Count == 1)
            {
                user = users[0];
                this.Resources.Add("user", user);
                user.updateFIO();
                enable = true;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            stackpanelLogin.Visibility = Visibility.Visible;
            text1.Visibility = Visibility.Hidden;
            text2.Visibility = Visibility.Hidden;
            text3.Visibility = Visibility.Hidden;
        }

        private void btn1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                LoadMainWindow(sender,  e);
            }
        }
    }
}
