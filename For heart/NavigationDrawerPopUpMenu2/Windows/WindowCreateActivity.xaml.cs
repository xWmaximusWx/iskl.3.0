﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ИСКО.Models;

namespace ИСКО.Windows
{
    /// <summary>
    /// Логика взаимодействия для WindowActivity.xaml
    /// </summary>
    public partial class WindowCreateActivity : Window
    {
        public Activity activity { get; set; }
        List<Course> CoursesList;
        public WindowCreateActivity()
        {
            InitializeComponent();
            activity = null;
            var client = new MongoClient("mongodb+srv://user:zOv1pGKMz0DJ4LfE@cluster0-at0y4.azure.mongodb.net/<dbname>?retryWrites=true&w=majority");
            var db = client.GetDatabase("SystemCorporateEdcation");
            var collection = db.GetCollection<Course>("Courses");
            CoursesList = collection.Find(mcomp => true).ToList();
            SortedSet<string> selectorsGroupe = new SortedSet<string>();
            CoursesList.ForEach(el => selectorsGroupe.Add(el.GroupOfCourse));
            selectGroupe.ItemsSource = selectorsGroupe;
            selectGroupe.SelectionChanged += SelectGroupe_SelectionChanged;
        }

        private void SelectGroupe_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var sel = CoursesList.FindAll(cl => cl.GroupOfCourse == (string)selectGroupe.SelectedItem);
            List<string> variant = new List<string>();
            sel.ForEach(it => variant.Add(it.NameOfCourse));
            selectCourse.ItemsSource = variant;
        }

        private void ButtonSave(object sender, RoutedEventArgs e)
        {
            var cr = CoursesList.Find(li => li.NameOfCourse == (string)selectCourse.SelectedItem);
            activity = new Activity
            {
                Name = cr.NameOfCourse,
                DataCloc = cr.ValueOfCourse,
                MethodEducation = cr.MethodOfEducation,
                StartActivity = (DateTime)DataPicerStart.SelectedDate,
                StopActivity = (DateTime)DataPicerStop.SelectedDate,
                Characteristic = cr.CompId[0].Value
            };
            this.Close();
        }


        private void ButtonClose(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
