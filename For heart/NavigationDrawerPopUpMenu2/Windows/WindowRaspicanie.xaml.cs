﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ИСКО.Models;

namespace ИСКО.Windows
{
    /// <summary>
    /// Логика взаимодействия для WindowRaspicanie.xaml
    /// </summary>
    public partial class WindowRaspicanie : Window
    {
        IndividualPlan _plan;
        public WindowRaspicanie(IndividualPlan plan)
        {
            InitializeComponent();
            if(plan != null)
            {
                _plan = plan;
                Calendar.SelectionMode = CalendarSelectionMode.MultipleRange;
                Calendar.SelectedDates.AddRange(_plan.PeriodStart, _plan.PeriodStop);
                //Calendar.SelectedDates =  _plan.PeriodStart,  _plan.PeriodStop; //new CalendarDateRange();
                
            }
                

        }

        private void calendar_SelectedDatesChanged(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("Hi");
        }
    }
}
