﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ИСКО.Models;

namespace ИСКО.Windows
{
    /// <summary>
    /// Логика взаимодействия для WindowCreateNewLearningPlain.xaml
    /// </summary>
    public partial class WindowCreateNewLearningPlain : Window
    {
        WindowCreateActivity Page;
        User us;
        List<Activity> actList;
        MongoClient client;
        IMongoDatabase db;
        IndividualPlan IndividualPlan;
        public WindowCreateNewLearningPlain(User user)
        {
            InitializeComponent();

            client = new MongoClient("mongodb+srv://user:zOv1pGKMz0DJ4LfE@cluster0-at0y4.azure.mongodb.net/<dbname>?retryWrites=true&w=majority");
            db = client.GetDatabase("SystemCorporateEdcation");
            var collection = db.GetCollection<User>("Users");
            List<string> admins = new List<string>();
            collection.Find(us => us.Status == true).ToList().ForEach(a => { a.updateFIO(); admins.Add(a.FIO); });
            Otvetstven.ItemsSource = admins;
            us = user;
            actList = new List<Activity>();
            if(user != null)
            {
                PersonName.Text = user.FIO;
                PersonDepartment.Text = user.Department;
                PersonPosition.Text = user.Position;
            }
            delBtnPlan.Visibility = Visibility.Hidden;
        }

        public WindowCreateNewLearningPlain(User user, IndividualPlan plan)
        {
            InitializeComponent();

            client = new MongoClient("mongodb+srv://user:zOv1pGKMz0DJ4LfE@cluster0-at0y4.azure.mongodb.net/<dbname>?retryWrites=true&w=majority");
            db = client.GetDatabase("SystemCorporateEdcation");
            var collection = db.GetCollection<User>("Users");
            List<string> admins = new List<string>();
            collection.Find(us => us.Status == true).ToList().ForEach(a => { a.updateFIO(); admins.Add(a.FIO); });
            Otvetstven.ItemsSource = admins;
            us = user;
            actList = new List<Activity>();
            if (user != null)
            {
                PersonName.Text = user.FIO;
                PersonDepartment.Text = user.Department;
                PersonPosition.Text = user.Position;
            }

            Recomend.Text = plan.Recomendation;
            Komments.Text = plan.Komments;
            Otvetstven.SelectedItem = plan.Responsible;

            foreach(var act in plan.activities)
            {
                DataGridActivity.Items.Add(new ActivityThis(act));
            }

            IndivPlanStart.SelectedDate = plan.PeriodStart;
            IndivPlanStop.SelectedDate = plan.PeriodStop;
            IndividualPlan = plan;
        }


        public class ActivityThis
        {
            public string Name { get; set; }
            public string DataCloc { get; set; }
            public string MethodEducation { get; set; }
            public string StartActivity { get; set; }
            public string StopActivity { get; set; }
            public string Characteristic { get; set; }

            public ActivityThis(Activity activity)
            {
                this.Name = activity.Name;
                this.DataCloc = string.Format("           {0}     ", activity.DataCloc.ToString());
                this.MethodEducation = activity.MethodEducation;
                this.StartActivity = activity.StartActivity.ToString("dd.mm.yyyy");
                this.StopActivity = activity.StopActivity.ToString("dd.mm.yyyy");
                this.Characteristic = activity.Characteristic;
            }

        }

        private void Button_Close(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void CreateActivity(object sender, RoutedEventArgs e)
        {
            Page = new WindowCreateActivity();
            Page.Show();
            Page.Closed += CreateActivityClose;
        }

        private void CreateActivityClose(object sender, EventArgs e)
        {
            var act =  Page.activity;
            if(act != null)
            {
                actList.Add(act);
                ActivityThis activity1 = new ActivityThis(act);
                DataGridActivity.Items.Add(activity1);
            }
        }

        private void DropActivity(object sender, RoutedEventArgs e)
        {
            DataGridActivity.Items.Remove(DataGridActivity.SelectedItem);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            IndividualPlan individualPlan = new IndividualPlan
            {
                Recomendation = Recomend.Text,
                PeriodStart = (DateTime)IndivPlanStart.SelectedDate,
                PeriodStop = (DateTime)IndivPlanStop.SelectedDate,
                Komments = Komments.Text,
                UserId = us.Id,
                Responsible = (string)Otvetstven.SelectedItem
            };

            individualPlan.activities = actList;
            var coll = db.GetCollection<IndividualPlan>("IndividualPlans");

            if(IndividualPlan == null)
            {
                coll.InsertOne(individualPlan);
            } else
            {
                coll.ReplaceOne(ip => ip.Id == IndividualPlan.Id, individualPlan);
            }
                
            this.Close();
        }

        private void Button_DeletePlan(object sender, RoutedEventArgs e)
        {
            var coll = db.GetCollection<IndividualPlan>("IndividualPlans");
            coll.DeleteOne(ip => ip.Id == IndividualPlan.Id);
            this.Close();
        }
    }
}
