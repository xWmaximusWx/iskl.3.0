﻿using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ИСКО.Models;

namespace ИСКО.Windows
{
    /// <summary>
    /// Логика взаимодействия для WindowCreateCourse.xaml
    /// </summary>
    public partial class WindowCreateCourse : Window
    {
        string link;
        ComboBox cmb1, cmb2;
        List<string> competense;
        MongoClient client;
        List<MetaCompetence> ListMetaCompetenses;
        public WindowCreateCourse()
        {
            InitializeComponent();
            competense = new List<string>();
        }

        /// <summary>
        ///  Сохранение пользователя
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {

            float fl = float.Parse(ValueSetter.Text);

            string goc = (string)NameGroupSetter.SelectedValue;
            string tod = (string)(TypeOfDocumentsReturned.SelectedItem as ComboBoxItem).Content;
            string moe = (string)(TypeOfMethodEdu.SelectedItem as ComboBoxItem).Content;


            Course course = new Course
            {
                NameOfCourse = NameCourseSetter.Text,
                GroupOfCourse = goc,
                Description = DeskriptionOfCourse.Text,
                ValueOfCourse = fl,
                TypeOfDocument = tod,
                MethodOfEducation = moe,
                Link = link,
               
            };
            

            string a = course.Description;
            course.CompId = new List<KeyValuePair<string,string>>();

            for (int i = 0; i < competense.Count; i+=2)
            {
                string mecomp = competense[i];
                string comp = competense[i + 1];
                course.CompId.Add(new KeyValuePair<string, string>(mecomp, comp));
            }


            var db = client.GetDatabase("SystemCorporateEdcation");
            var courses = db.GetCollection<Course>("Courses");
            courses.InsertOne(course);

            this.Close();
        }

        /// <summary>
        /// Закрытие формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void OpendDialog(object sender, RoutedEventArgs e)
        {
            var pg = new OpenFileDialog();
            pg.ShowDialog();
            string fileName = pg.FileName;
            link = fileName;
            CouseViews.Children.Clear();
            var stack = new StackPanel
            {
                Orientation = Orientation.Horizontal,
            };

            stack.Children.Add(new TextBlock
            {
                Height = 20,
                Text = fileName,
            });


            var btn = new Button
            {
                Foreground = Brushes.Red,
                Background = null,
                Width = 30,
                Height = 30,
                BorderBrush = null,
                Margin = new Thickness(350, 0, 0, 0),


                Content = new PackIcon
                {
                    Margin = new Thickness(-18),
                    Kind = PackIconKind.Rubbish,
                    Width = 28,
                    Height = 34
                }
            };

            btn.Click += DropCourseFile;

            stack.Children.Add(btn);

            CouseViews.Children.Add(stack);

        }

        private void DropCourseFile(object sender, RoutedEventArgs e)
        {
            CouseViews.Children.Clear();

            var btn = new Button
            {

                Width = 80,
                Content = new PackIcon
                {
                    
                    Kind = PackIconKind.Plus,
                }
            };

            btn.Click += OpendDialog;
            CouseViews.Children.Add(btn);
        }

        private void CourseAddCompetense(object sender, RoutedEventArgs e)
        {
            var rootStak = ((sender as Button).Parent as StackPanel);
            rootStak.Children.Clear();

            cmb1 = new ComboBox
            {
                Width = 270,
                Margin = new Thickness(20, 3, 20, 3)
            };

            client = new MongoClient("mongodb+srv://user:zOv1pGKMz0DJ4LfE@cluster0-at0y4.azure.mongodb.net/<dbname>?retryWrites=true&w=majority");
            var db = client.GetDatabase("SystemCorporateEdcation");
            ListMetaCompetenses = db.GetCollection<MetaCompetence>("MetaCompetences").Find(comp => true).ToList();

            List<string> listMetaString = new List<string>();
            ListMetaCompetenses.ForEach(lmc => listMetaString.Add(lmc.MetaСompetenceName));


            cmb1.ItemsSource = listMetaString;
            cmb1.SelectionChanged += SelectMetaCompetence;



            cmb2 = new ComboBox
            {
                Width = 270,
                Margin = new Thickness(20, 3, 20, 3)
            };

            var delBtn = new Button
            {

                Foreground = Brushes.Red,
                Background = null,
                BorderBrush = null,
                Margin = new Thickness(30, 0, 0, 0),
                Content = new PackIcon
                {
                    Kind = PackIconKind.Rubbish,
                    Width = 20
                }
            };

            delBtn.Click += DelBtn_Click;


            rootStak.Children.Add(cmb1);
            rootStak.Children.Add(cmb2);
            rootStak.Children.Add(delBtn);


            var statChild = new StackPanel
            {
                HorizontalAlignment = HorizontalAlignment.Left,
                Orientation = Orientation.Horizontal
            };


            var btn = new Button
            {
                Width = 80,
                Content = new PackIcon
                {
                    Kind = PackIconKind.Plus,
                    Width = 30,
                    Height = 30
                }
            };

            btn.Click += CourseAddCompetense;
            statChild.Children.Add(btn);
            (rootStak.Parent as StackPanel).Children.Add(statChild);

        }

        private void SelectMetaCompetence(object sender, SelectionChangedEventArgs e)
        {
            string state = (string)(sender as ComboBox).SelectedItem;

            List<string> lcomp = new List<string>();
            ListMetaCompetenses.Find(ls => ls.MetaСompetenceName == state).competences.FindAll(cm => true).ForEach(cm => lcomp.Add(cm.СompetenceName));
            cmb2.ItemsSource = lcomp;
            cmb2.SelectionChanged += SelectCompetence;

            
        }

        private void SelectCompetence(object sender, SelectionChangedEventArgs e)
        {
            string met = (string)cmb1.SelectedValue;
            string cmp = (string)cmb2.SelectedValue;

            competense.Add(met);
            competense.Add(cmp);
        }

        private void DelBtn_Click(object sender, RoutedEventArgs e)
        {
            var rootBtn = ((sender as Button).Parent as StackPanel);

            int index = (rootBtn.Parent as StackPanel).Children.IndexOf(rootBtn);
            (rootBtn.Parent as StackPanel).Children.RemoveAt(index);

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> groups = new List<string>();
            groups.Add("Курсы IT");
            groups.Add("Курсы менеджмента");
            groups.Add("Курсы управления персоналом");
            groups.Add("Курсы маркетинга и рекламы");
            groups.Add("Курсы менчендайзера");
            groups.Add("Курсы управления финансами");

            NameGroupSetter.ItemsSource = groups;

        }
    }
}
