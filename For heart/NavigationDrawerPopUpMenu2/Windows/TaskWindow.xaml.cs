﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using NavigationDrawerPopUpMenu2;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ИСКО.UserControls;

namespace ИСКО.UserControls
{
    /// <summary>
    /// Логика взаимодействия для TaskWindow.xaml
    /// </summary>
    public partial class TaskWindow : Window
    {
        public UserControlProfilCompeten pwin;
        public void setpwin(UserControlProfilCompeten pw) 
        {
            pwin = pw;
        }
        public TaskWindow()
        {

            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (pwin.CollectionCompetence.Find(MetaCompetence => MetaCompetence.MetaСompetenceName.Contains( NameMeta.Text)).ToList<MetaCompetence>().Count < 1)
            {
                pwin.SaveMeta(NameMeta.Text, NameMeta_Copy.Text);
                pwin.LoadListCompetence();
                this.Close();
            }
            else
            {
                //такая компитенция уже существует

            }
        }
    }
}
