﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MongoDB.Bson;
using MongoDB.Driver;
using NavigationDrawerPopUpMenu2;
using ИСКО;

namespace NavigationDrawerPopUpMenu2
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindowUser : Window
    {

        public User user;
        public MainWindowUser()
        {
            InitializeComponent();
        }
        public MainWindowUser(User us)
        {
            InitializeComponent();
            user = us;
            this.DataContext = user;
            GridMain.Children.Add(new UserControlSotrudnic(user));
        }
        
        private void ButtonOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonCloseMenu.Visibility = Visibility.Visible;
            ButtonOpenMenu.Visibility = Visibility.Collapsed;
        }

        private void ButtonCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonCloseMenu.Visibility = Visibility.Collapsed;
            ButtonOpenMenu.Visibility = Visibility.Visible;
        }

        private void ListViewMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UserControl usc = null;
            GridMain.Children.Clear();

            switch (((ListViewItem)((ListView)sender).SelectedItem).Name)
            {
                case "ItemHome":
                    usc = new UserControlHome();
                    GridMain.Children.Add(usc);
                    break;
                case "ItemCreate":
                    usc = new UserControlCreate();
                    GridMain.Children.Add(usc);
                    break;
                default:
                    break;
            }
        }

        private void CloseWindov_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Select_Click(object sender, RoutedEventArgs e)
        {
            int index = 0;
            try
            {
                index = int.Parse(((ListViewItem)(e.Source)).Uid);
            }
            catch(Exception ex) {

            }

            switch (index)
            {
                case 0:
                    GridMain.Children.Clear();
                    GridMain.Children.Add(new UserControlSotrudnic(user));
                    break;
                case 1:
                    GridMain.Children.Clear();
                    GridMain.Children.Add(new UserControlProfilUspeshnosti());
                    break;
                case 2:
                    GridMain.Children.Clear();
                    GridMain.Children.Add(new UserControlProfilCompeten());
                    break;
                case 3:
                    GridMain.Children.Clear();
                    GridMain.Children.Add(new UserControlTests());
                    break;
                case 4:
                    GridMain.Children.Clear();
                    GridMain.Children.Add(new UserControlGrades());
                    break;
                case 5:
                    GridMain.Children.Clear();
                    GridMain.Children.Add(new UserControlLearning(user));
                    break;
                case 6:
                    GridMain.Children.Clear();
                    GridMain.Children.Add(new UserControlChat(user));
                    break;
            } 
        }
    }
}
