﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ИСКО.UserControls
{
    /// <summary>
    /// Логика взаимодействия для TaskWindow2.xaml
    /// </summary>
    public partial class TaskWindow2 : Window
    {
        public class Scaling
        {
            public string Name;
            public int Min, Max;
            public Scaling(string name, int min, int max)
            {
                this.Name = name;
                this.Min = min;
                this.Max = max;
            }
        }

        public List<Scaling> ListScaling = new List<Scaling>();
        public List<WorkScalBall> SaveWCB = new List<WorkScalBall>();
        public UserControlProfilCompeten pwin;
        public void setpwin(UserControlProfilCompeten pw)
        {
            pwin = pw;
        }
        public TaskWindow2()
        {
            InitializeComponent();
                ListScaling.Add(new Scaling("Пятибальная",1,5));
                ListScaling.Add(new Scaling("Десятибальная", 1, 10));
                ListScaling.Add(new Scaling("Стобальная", 1, 100));
        } 
        public  WorkScalBall WSB = new WorkScalBall();
        private void Button_Click_1(object sender, RoutedEventArgs e)//сохранить
        {
           
           //
            pwin.SaveCompetence(Read_NameCompetence.Text, Read_DefinitionCompetence.Text, SaveWCB,Select_Meta.SelectedItem.ToString());
            pwin.LoadListCompetence();
            this.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)//отмена
        {
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)//записать
        {
            bool fP, fS, fB;
            fP = Select_Position.SelectedItem != null;
            fS =Select_Scaling.SelectedItem != null;
            fB= Read_Ball.Text != null;

            string msg = "Введите все значения для записи!";

            if (fP && fS && fB)
            { int x;
                if (int.TryParse(Read_Ball.Text, out x))
                {
                    Scaling scal = ListScaling.Find(SelectScal => SelectScal.Name == Select_Scaling.Text);
                    if (x <= scal.Max && x > scal.Min)
                    {
                        WSB.Position = Select_Position.SelectedItem.ToString();
                        WSB.Balss = Read_Ball.Text;
                        WSB.scaling = Select_Scaling.SelectedItem.ToString();
                        SaveWCB.Add(WSB);//Добавляем в список
                        PosScalBall.Items.Add(WSB);
                        return;
                    }
                    else
                    {
                        msg = "Значение оценки выходит за рамки выбранной шкалы";
                    }
                }
                else
                {
                    msg = "Оценка должна быть вырожена числом!";
                }
            }
            else
            {
                MessageBox.Show(msg);
            }
            MessageBox.Show(msg);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)//удалить
        {
            //List<WorkScalBall> psb =( List < WorkScalBall > )PosScalBall.ItemsSource;
            SaveWCB.Remove(SaveWCB.Find(x => x.Position == SaveWCB[PosScalBall.SelectedIndex].Position  ));
            PosScalBall.Items.Remove(PosScalBall.SelectedItem);
        }

        private void Select_Scaling_Loaded(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < ListScaling.Count; i++)
            {
                Select_Scaling.Items.Add(new TextBox().Text = ListScaling[i].Name);
            }
            Select_Scaling.SelectedIndex =0;
        }

        private void Select_Position_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
