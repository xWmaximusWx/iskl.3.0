﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ИСКО.Windows
{
    /// <summary>
    /// Логика взаимодействия для WindowCreateEmployee.xaml
    /// </summary>
    public partial class WindowCreateEmployee : Window
    {
        public WindowCreateEmployee()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Save_Emplayee(object sender, RoutedEventArgs e)
        {
            if(LoginSetter.Text != null && PasswordSetter.Text != null && FNameSetter.Text != null )
            {
                var client = new MongoClient("mongodb+srv://user:zOv1pGKMz0DJ4LfE@cluster0-at0y4.azure.mongodb.net/<dbname>?retryWrites=true&w=majority");
                var db = client.GetDatabase("SystemCorporateEdcation");
                var collection = db.GetCollection<User>("Users");
                User user = new User
                {
                    Login = LoginSetter.Text,
                    Password = PasswordSetter.Text,
                    FName = FNameSetter.Text,
                    MName = MNameSetter.Text,
                    LName = LNameSetter.Text,
                    Department = DepartmentSetter.Text,
                    Position = WorkPositiontSetter.Text,
                    PhoneNumber = PhoneNumberSetter.Text,
                    Status = false,
                    Email = EmailSetter.Text,
                    Work = "Торговая компания ANY-TRADE",
                    Imgage = "https://sun9-18.userapi.com/HNYSMmQroH-62oX5zBEUl0JQppj9a3RUXLah9g/_5h9zb8UX9E.jpg",
                };
                collection.InsertOne(user);
            }
            
            this.Close();
        }
        
    }
}
