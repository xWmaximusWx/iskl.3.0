﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ИСКО.UserControls
{
    /// <summary>
    /// Логика взаимодействия для UserControlAboutUser.xaml
    /// </summary>
    public partial class UserControlAboutUser : UserControl
    {
        User user;
        public UserControlAboutUser()
        {
            InitializeComponent();
        }


        public UserControlAboutUser(User us)
        {
            InitializeComponent();
            user = us;
            string url = user.Imgage;
            WebRequest request = WebRequest.Create(url);
            WebResponse response = request.GetResponse();
            System.IO.Stream responseStream = response.GetResponseStream();
            BitmapImage image = null;
            image = new BitmapImage();
            image.BeginInit();
            image.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
            image.StreamSource = responseStream;
            image.EndInit();
            imagePanel.Source = image;


            user.updateFIO();
            UserName.Text = user.FIO;
            PhoneNumber.Text = user.PhoneNumber;
            Email.Text = user.Email;
            Work.Text = user.Work;
            Department.Text = user.Department;
            Position.Text = user.Position;

            // Ned edit !!!!!!!!!
            СolCourse.Text = "0";
            Sertefictes.Text = "0";
        }
    }
}
