﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ИСКО.Models;
using ИСКО.Windows;

namespace ИСКО.UserControls
{
    /// <summary>
    /// Логика взаимодействия для UserControlCourseEditing.xaml
    /// </summary>
    public partial class UserControlCourseEditing : UserControl
    {

        MongoClient client;
        List<Course> coursesAll;
        HashSet<string> group;

        public UserControlCourseEditing()
        {
            InitializeComponent();
            client = new MongoClient("mongodb+srv://user:zOv1pGKMz0DJ4LfE@cluster0-at0y4.azure.mongodb.net/<dbname>?retryWrites=true&w=majority");
            var db = client.GetDatabase("SystemCorporateEdcation");
            coursesAll = db.GetCollection<Course>("Courses").Find(c => true).ToList();
        }

        private void CreatCourse(object sender, RoutedEventArgs e)
        {
            //var item = new TreeViewItem()
            //{
            //    Header = "Group"
            //};

            //item.Items.Add(null);

            //CoursesGroups.Items.Add(item);
            var createCoursePage = new WindowCreateCourse();
            createCoursePage.Show();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            group = new HashSet<string>();
            coursesAll.ForEach(comp => group.Add(comp.GroupOfCourse));
            foreach (var gr in group)
            {
                var tree = new TreeViewItem()
                {
                    Header = gr,
                    Tag = gr
                };
                tree.Items.Add(null);
                tree.Expanded += Group_Expanded;
                CoursesGroups.Items.Add(tree);
            } 
        }

        private void Group_Expanded(object sender, RoutedEventArgs e)
        {
            var root = (sender as TreeViewItem);

            root.Items.Clear();
            List<Course> list = coursesAll.FindAll(cm => cm.GroupOfCourse == (string)root.Tag);
            list.ForEach(comp =>
            {
                var subItem = new TreeViewItem
                {
                    Header = comp.NameOfCourse,
                    Tag = comp.NameOfCourse,
                    ContextMenu = new ContextMenu()
                };

                var contMen = new MenuItem
                {
                    Header = "Удалить",
                    Tag = comp.NameOfCourse,
                };

                contMen.Click += DeleteCourse_Click;

                subItem.ContextMenu.Items.Add(contMen);
               

                subItem.Selected += Course_Selected;
                root.Items.Add(subItem);
            }
            );
        }

        private void DeleteCourse_Click(object sender, RoutedEventArgs e)
        {
            string delName = (string)(sender as MenuItem).Tag;
            
            var db = client.GetDatabase("SystemCorporateEdcation");
            var deleteCourse = db.GetCollection<Course>("Courses");
            deleteCourse.DeleteOne(delc => delc.NameOfCourse == delName);
        }

        private void Course_Selected(object sender, RoutedEventArgs e)
        {
            string course = (string)(sender as TreeViewItem).Tag;
            var param = coursesAll.Find(cr => cr.NameOfCourse == course);
            CourseDescriptionGrid.Children.Clear();
            CourseDescriptionGrid.Children.Add(new UserControlCourseDescription(param));
        }
    }
}
