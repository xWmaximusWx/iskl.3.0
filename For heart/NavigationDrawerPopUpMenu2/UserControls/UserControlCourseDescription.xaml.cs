﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ИСКО.Models;

namespace ИСКО.UserControls
{
    /// <summary>
    /// Логика взаимодействия для UserControlCourseDescription.xaml
    /// </summary>
    public partial class UserControlCourseDescription : UserControl
    {
        public UserControlCourseDescription(Course cr)
        {
            InitializeComponent();
            NameOfCourse.Text = cr.NameOfCourse;
            DescriptionOfCourse.Text = cr.Description;
            ValueOfCourse.Text = cr.ValueOfCourse.ToString();
            MethoOfCourse.Text = cr.MethodOfEducation;
            string comp = "";
            cr.CompId.ForEach(k => comp += string.Format("{0}:     {1} \n",k.Key, k.Value));
            CompetenseList.Text = comp;
        }
    }
}
