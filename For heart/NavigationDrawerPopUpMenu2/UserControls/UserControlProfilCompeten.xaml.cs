﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using NavigationDrawerPopUpMenu2;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ИСКО.UserControls;


namespace ИСКО
{
    /// <summary>
    /// Логика взаимодействия для UserControlProfilCompeten.xaml
    /// </summary>
    public partial class UserControlProfilCompeten : UserControl
    {
        public MongoClient client;
        public IMongoDatabase db;
        public IMongoCollection<User> collection;
        public IMongoCollection<MetaCompetence> CollectionCompetence;
        static bool enable;
        public static User user { get; set; }
        public static Competence competence { get; set; }
        
        public UserControlProfilCompeten()
        {
            InitializeComponent();
            enable = false;

            client = new MongoClient("mongodb+srv://user:zOv1pGKMz0DJ4LfE@cluster0-at0y4.azure.mongodb.net/<dbname>?retryWrites=true&w=majority");
            db = client.GetDatabase("SystemCorporateEdcation");
            collection = db.GetCollection<User>("Users");
            CollectionCompetence = db.GetCollection<MetaCompetence>("MetaCompetences");
        }

        public class UserFIO
        {
            public string FName { get; set; }
            public string LName { get; set; }
            public string MName { get; set; }
            public string PhoneNumber { get; set; }

            public UserFIO(string _FName, string _LName, string _MName, string _PhoneNumber)
            {
                this.FName = _FName;
                this.LName = _LName;
                this.MName = _MName;
                this.PhoneNumber = _PhoneNumber;
            }
        }
        public List<UserFIO> ShowUsers = new List<UserFIO>();
        public List<User> userfilt = new List<User>();

        public void LoadUser(List<UserFIO> _user)
        {
            UserList.Items.Clear(); // очищаем лист с элементами

            for (int i = 0; i < _user.Count; i++) // перебираем элементы
            {
                UserList.Items.Add(_user[i]); // добавляем элементы в ListBox
            }
        }
        private List<UserFIO> GetDBData(string Filtre)
        {
            List<User> BufList = new List<User>();
            userfilt = collection.Find(user => true).ToList();
            List<UserFIO> newUsers = new List<UserFIO>();
            if (Filtre != "")
            {
                BufList = userfilt.FindAll(x => x.FName.Contains(Filtre));
                BufList.AddRange(userfilt.FindAll(x => x.LName.Contains(Filtre)));
                BufList.AddRange(userfilt.FindAll(x => x.MName.Contains(Filtre)));
                BufList.AddRange(userfilt.FindAll(x => x.PhoneNumber.Contains(Filtre)));
                userfilt = BufList;
            }
            for (int i = 0; i < userfilt.Count; i++) // перебираем элементы
            {
                newUsers.Add(new UserFIO(userfilt[i].FName, userfilt[i].LName, userfilt[i].MName, userfilt[i].PhoneNumber));
            }
            ShowUsers = newUsers;
            return (newUsers);
        }

        private void ActiveFilter(object sender, RoutedEventArgs e)
        {
            
            LoadUser(GetDBData(NameFiltre.Text));
        }

        private void UserList_Loaded(object sender, RoutedEventArgs e)
        {
            if (NameFiltre.Text == "")
            { LoadUser(GetDBData("")); }
            LoadListCompetence();

        }

        private void UserList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            TaskWindow taskWindow = new TaskWindow();
            taskWindow.Show();
            taskWindow.setpwin(this);

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)//Создать новую компитенцию
        {
            TaskWindow2 taskWindow = new TaskWindow2();//экзепляр диалогового окна редактирования компитенции 
            taskWindow.Show();// отображает диалоговое окно
            taskWindow.setpwin(this);//передает ссылку на этот лбъект
         
           
            List<MetaCompetence> LoadCompetence = new List<MetaCompetence>();// переменная для хранения списка всех компетенций
            LoadCompetence = CollectionCompetence.Find(competence => true).ToList();// получение колекции компитенций
            try// на случай пустой колекции
            {
                for (int i = 0; i < LoadCompetence.Count; i++) // перебираем элементы
                {
                    taskWindow.Select_Meta.Items.Add(new TextBox().Text = LoadCompetence[i].MetaСompetenceName);// передаем название мета копетенций для списка в деалоговом окне
                }
            }

            catch
            {
            }

            List<User> LoadProf= new List<User>();// переменная для хранения списка всех компетенций
            LoadProf = collection.Find(User => true).ToList();// получение колекции компитенций
            try// на случай пустой колекции
            {
                for (int i = 0; i < LoadProf.Count; i++) // перебираем элементы
                {
                    taskWindow.Select_Position.Items.Add(new TextBox().Text = LoadProf[i].Position);// передаем название мета копетенций для списка в деалоговом окне
                }
            }
            catch
            {
            }

            taskWindow.Select_Position.SelectedIndex = 0;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {

        }
        public void LoadListCompetence() // отображение компитенций в treeview
        {
            ListCompetence.Items.Clear(); // очистка списка
            TreeViewItem Item, SabItem;        // переменые хранящие элементы и элементы нижнего уровня
            List<MetaCompetence> LoadCompetence = new List<MetaCompetence>(); // список мета копетенций 
            LoadCompetence = CollectionCompetence.Find(competence => true).ToList();// получение всех метакомпетенций
            try
            {
                for (int i = 0; i < LoadCompetence.Count; i++) // перебираем элементы
                {
                    Item = new TreeViewItem();
                    Item.Header = LoadCompetence[i].MetaСompetenceName;
                    Item.Tag = "M" + i;
                    ListCompetence.Items.Add(Item);
                    try
                    {
                        if (LoadCompetence[i].competences != null)
                        {
                            for (int j = 0; j < LoadCompetence[i].competences.Count; j++)

                            {
                                SabItem = new TreeViewItem();
                                SabItem.Tag = "C" + i;
                                SabItem.Header = LoadCompetence[i].competences[j].СompetenceName;
                                Item.Items.Add(SabItem);
                            }
                        }
                    }
                    catch { }
                }
            }
            catch { }
        }
        public void SaveCompetence(string name, string definition,List<WorkScalBall> skaling,string MetaName) 
        {
           List< MetaCompetence> updateMeta = CollectionCompetence.Find(MetaCompetence => MetaCompetence.MetaСompetenceName == MetaName).ToList<MetaCompetence>();
            Competence saveComp = new Competence();
            saveComp.СompetenceName = name;
            saveComp.Definition = definition;
            saveComp.workScalBall = skaling;
            saveComp.MetaName = MetaName;
            try
            {
                updateMeta[0].competences.Add(saveComp);
            }catch
            {
                updateMeta[0].competences = new List<Competence>();
                updateMeta[0].competences.Add(saveComp);
            }
            CollectionCompetence.ReplaceOneAsync(Meta => Meta.MetaСompetenceName == MetaName, updateMeta[0]);
            

            
        }
        public void SaveMeta(string name, string definition) 
        {
            BsonDocument dock = new BsonDocument { { "MetaСompetenceName", name }, { "MetaDefinition", definition } };
            MetaCompetence metasave = BsonSerializer.Deserialize<MetaCompetence>(dock);
            CollectionCompetence.InsertOneAsync(metasave);

        }

        private void Button_Click_3(object sender, RoutedEventArgs e)// кнопка удалить
        { 
            bool br =false;
            foreach (TreeViewItem it in ListCompetence.Items)
            {
                if (ListCompetence.SelectedItem == it)
                {
                        CollectionCompetence.DeleteOne(MetaCompetence => MetaCompetence.MetaСompetenceName == ((TreeViewItem)ListCompetence.SelectedItem).Header.ToString());// удаляем из базы данных
                    break;
                }
                else
                {
                    foreach (TreeViewItem Subit in it.Items)
                    {
                        if (ListCompetence.SelectedItem == Subit)
                        {
                            List<MetaCompetence> Mcom = CollectionCompetence.Find(MetaCompetence => MetaCompetence.MetaСompetenceName == it.Header.ToString()).ToList<MetaCompetence>();
                            List<Competence> com = Mcom[0].competences;
                            com.Remove(Mcom[0].competences.Find(x => x.СompetenceName == ((TreeViewItem)ListCompetence.SelectedItem).Header.ToString()));
                            Mcom[0].competences = com;
                            CollectionCompetence.ReplaceOneAsync(Meta => Meta.MetaСompetenceName == it.Header.ToString(), Mcom[0]);
                            br = true;
                            break;
                        }
                    }
                    if (br) break;
                }  
            }
            LoadListCompetence();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            if (UserList.SelectedItem != null)
            {
                
                Purpose Purp = new Purpose();//экзепляр диалогового окна назначения компитенций
                Purp.Show();// отображает диалоговое окно
                List<User> usBUF = new List<User>();
                usBUF = collection.Find(x => x.FName == ShowUsers[UserList.SelectedIndex].FName && x.LName == ShowUsers[UserList.SelectedIndex].LName && x.MName == ShowUsers[UserList.SelectedIndex].MName).ToList();
                Purp.setpwin(this, usBUF[0]);//передает ссылку на этот лбъект
                Purp.LoadChange();
            }
            else
            {
                MessageBox.Show("Выберите сотрудника, которому хотите назначить компитенции");
            }
        }
    }
    
}

