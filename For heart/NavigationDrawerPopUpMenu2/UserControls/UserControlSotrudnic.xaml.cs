﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using ИСКО.Windows;
using MongoDB.Driver;
using ИСКО.Models;

namespace ИСКО
{
    /// <summary>
    /// Логика взаимодействия для UserControlSotrudnic.xaml
    /// </summary>
    public partial class UserControlSotrudnic : UserControl
    {
        public UserControlSotrudnic(User us)
        {
            InitializeComponent();
            string url = us.Imgage;
            WebRequest request = WebRequest.Create(url);
            WebResponse response = request.GetResponse();
            System.IO.Stream responseStream = response.GetResponseStream();
            BitmapImage image = null;
            image = new BitmapImage();
            image.BeginInit();
            image.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
            image.StreamSource = responseStream;
            image.EndInit();
            imgPanel.Source = image;

            if (us.Status)
            {
                StatusTitle.Text = "Администратор";
                btnRasp.Visibility = Visibility.Hidden;
                
            }
            else { StatusTitle.Text = "Сотрудник"; btnRasp.Visibility = Visibility.Visible; } 
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var client = new MongoClient("mongodb+srv://user:zOv1pGKMz0DJ4LfE@cluster0-at0y4.azure.mongodb.net/<dbname>?retryWrites=true&w=majority");
            var db = client.GetDatabase("SystemCorporateEdcation");
            var plan = db.GetCollection<IndividualPlan>("IndividualPlans").Find(pl => true).ToList();
            var page = new WindowRaspicanie(plan[0]);
            page.Show();
        }
    }
}
