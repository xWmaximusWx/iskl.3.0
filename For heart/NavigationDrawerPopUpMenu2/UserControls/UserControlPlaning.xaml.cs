﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ИСКО.UserControls
{
    /// <summary>
    /// Логика взаимодействия для UserControlPlaning.xaml
    /// </summary>
    public partial class UserControlPlaning : UserControl
    {
        User user;
        public UserControlPlaning()
        {
            InitializeComponent();
        }

        public UserControlPlaning(User us)
        {
            InitializeComponent();
            user = us;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainGrid.ColumnDefinitions.Clear();
            MainGrid.Children.Clear();
            MainGrid.Children.Add(new UserControlGroupEditing(user));
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MainGrid.ColumnDefinitions.Clear();
            MainGrid.Children.Clear();
            MainGrid.Children.Add(new UserControlCourseEditing());
        }
    }
}
