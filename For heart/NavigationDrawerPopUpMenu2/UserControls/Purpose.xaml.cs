﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using NavigationDrawerPopUpMenu2;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ИСКО.UserControls;

namespace ИСКО.UserControls
{
    /// <summary>
    /// Логика взаимодействия для Purpose.xaml
    /// </summary>
    public partial class Purpose : Window
    {
        public UserControlProfilCompeten pwin;
        User updateUser = new User();
        public void setpwin(UserControlProfilCompeten pw, User Pers)
        {
            pwin = pw;
            updateUser = Pers;
        }

        public MongoClient client;
        public IMongoDatabase db;
        public IMongoCollection<User> collection;
        public IMongoCollection<MetaCompetence> CollectionCompetence;
       public List<MetaCompetence> LoadCompetence = new List<MetaCompetence>(); // список мета копетенций 
        public List<Competence> ChangedCompetence = new List<Competence>(); // список назначаемых крмпитенций
        public Purpose()
            {
                InitializeComponent();
            
                client = new MongoClient("mongodb+srv://user:zOv1pGKMz0DJ4LfE@cluster0-at0y4.azure.mongodb.net/<dbname>?retryWrites=true&w=majority");
                db = client.GetDatabase("SystemCorporateEdcation");
                collection = db.GetCollection<User>("Users");
                CollectionCompetence = db.GetCollection<MetaCompetence>("MetaCompetences");
           
            LoadCompetence = CollectionCompetence.Find(competence => true).ToList();// получение всех метакомпетенций
            //
            
        }
        public void LoadChange()
        {
            // Закоментировал из за изменения юзера
            //List<Competence> loadChanged = new List<Competence>();
            //loadChanged = updateUser.assignedCompetence;
            //if (loadChanged != null)
            //{
            //    foreach (Competence CompLoad in loadChanged)
            //    {
            //        FillListCompetenceChange(CompLoad.СompetenceName);

            //    }
            //}
        }
        private void AddComp_Click(object sender, RoutedEventArgs e)
        {
            if (ListCompetenceAll.SelectedItem != null)
            {
                FillListCompetenceChange(((TreeViewItem)ListCompetenceAll.SelectedItem).Header.ToString());
            }
            else
            {
                MessageBox.Show("для того чтобы добавить компитенцию в список назначеных необходимо для начало выбрать необходимую компитенцию.");
            }
         }

        public void FillListCompetenceChange(string TARGET)
        {
            
               // MessageBox.Show("Есть выбранный элемент");
                bool br = false;
                foreach (TreeViewItem it in ListCompetenceAll.Items)
                {
                    if (TARGET == it.Header.ToString())//если это мета, то
                    {
                       // MessageBox.Show("будет вводиться мета");
                        bool non = false;
                        foreach (TreeViewItem it1 in ListCompetenceChange.Items)
                        {
                            if (it1.Header.ToString() == TARGET)
                            {
                                non = true;
                                break;
                            }
                        }

                        if (non)
                        {
                            //MessageBox.Show("Эта мета уже добавлена");

                        }
                        else
                        {

                            //MessageBox.Show("Такой меты нет");
                            TreeViewItem ITEM = new TreeViewItem();
                            ITEM.Header = TARGET;
                           // ITEM.Tag = ((TreeViewItem)ListCompetenceAll.SelectedItem).Tag;
                            ListCompetenceChange.Items.Add(ITEM);
                            List<Competence> thisList = LoadCompetence.Find(x => x.MetaСompetenceName == TARGET).competences;



                            if (thisList.Count != 0)
                            {
                                TreeViewItem SabItem;
                                for (int j = 0; j < thisList.Count; j++)

                                {
                                    SabItem = new TreeViewItem();
                                    SabItem.Tag = "C" + j;
                                    SabItem.Header = thisList[j].СompetenceName;
                                    ITEM.Items.Add(SabItem);
                                }
                            }


                        }

                    }
                    else// иначе проверяем компитенции
                    {
                        foreach (TreeViewItem Subit in it.Items)// перебираем внутри меты
                        {
                            if (Subit.Header.ToString() == TARGET)
                            {
                               // MessageBox.Show("Выбрана компитенция - " + TARGET);
                                MetaCompetence selectedMeta = new MetaCompetence();
                                selectedMeta = LoadCompetence.Find(x => x.MetaСompetenceName == it.Header.ToString());

                                TreeViewItem MetaForAdd = new TreeViewItem();
                                bool non = false, NonCom = false;
                                foreach (TreeViewItem it1 in ListCompetenceChange.Items)
                                {
                                    if (it1.Header == ((TreeViewItem)it).Header)
                                    {
                                        foreach (TreeViewItem it1sub in it1.Items)
                                        {
                                            if (it1sub.Header.ToString() == TARGET)
                                            {
                                                NonCom = true;//значит есть

                                            }
                                        }
                                        MetaForAdd = it1;
                                        non = true;
                                        break;
                                    }
                                }

                                if (!non)
                                {
                                    //MessageBox.Show("Меты для выбранной компитенции нет");

                                    TreeViewItem ITEM = new TreeViewItem();
                                    ITEM.Header = ((TreeViewItem)it).Header;
                                    ITEM.Tag = ((TreeViewItem)it).Tag;
                                    ListCompetenceChange.Items.Add(ITEM);

                                    TreeViewItem subITEM = new TreeViewItem();
                                    subITEM.Header =TARGET;
                                    //subITEM.Tag = ((TreeViewItem)ListCompetenceAll.SelectedItem).Tag;

                                    ITEM.Items.Add(subITEM);
                                }
                                else
                                {
                                   // MessageBox.Show("Мета для даной компитенции уже добавлена");
                                    if (NonCom)
                                    {
                                        //MessageBox.Show("Эта компитенция уже назначена");
                                    }
                                    else
                                    {
                                        TreeViewItem subITEM = new TreeViewItem();
                                        subITEM.Header = TARGET;
                                        //subITEM.Tag = ((TreeViewItem)ListCompetenceAll.SelectedItem).Tag;

                                        MetaForAdd.Items.Add(subITEM);

                                       // MessageBox.Show("Компитенция успешно добавлена");
                                    }

                                }
                            }
                        }
                        if (br) break;
                    }
                }
            

        }

        private void DeleteComp_Click(object sender, RoutedEventArgs e)
        {
            bool br = false;
            foreach (TreeViewItem it in ListCompetenceChange.Items)
            {
                if (ListCompetenceChange.SelectedItem == it)
                {
                    ListCompetenceChange.Items.Remove(ListCompetenceChange.SelectedItem);
                    break;
                }
                else
                {
                    foreach (TreeViewItem Subit in it.Items)
                    {
                        if (ListCompetenceChange.SelectedItem == Subit)
                        {
                            it.Items.Remove(Subit);
                            if ( it.Items.Count ==0 )
                            {
                                ListCompetenceChange.Items.Remove(it);
                            }
                            br = true;
                            break;
                        }
                    }
                    if (br) break;
                }
            }
        }

        private void SaveComp_Click(object sender, RoutedEventArgs e)
        {
            // закоментировал 
            //foreach (TreeViewItem it in ListCompetenceChange.Items)
            //{
            //    if (it != null)
            //    {
                    
            //        foreach (TreeViewItem Subit in it.Items)
            //        {
            //            if (Subit != null)
            //            {
            //                ChangedCompetence.Add(LoadCompetence.Find(x=> x.MetaСompetenceName == it.Header.ToString()).competences.Find(y=>y.СompetenceName == Subit.Header.ToString()));
                            
            //            }
            //        }
            //    }
            //}
            //updateUser.assignedCompetence = ChangedCompetence;
            //collection.ReplaceOneAsync(User => User.FName == updateUser.FName && User.LName == updateUser.LName && User.MName == updateUser.MName, updateUser);
            //this.Close();
        }

        private void CancelComp_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TreeView_Loaded(object sender, RoutedEventArgs e)
        {
            ListCompetenceAll.Items.Clear(); // очистка списка
            TreeViewItem Item, SabItem;        // переменые хранящие элементы и элементы нижнего уровня
            try
            {
                for (int i = 0; i < LoadCompetence.Count; i++) // перебираем элементы
                {
                    Item = new TreeViewItem();
                    Item.Header = LoadCompetence[i].MetaСompetenceName;
                    Item.Tag = "M" + i;
                    ListCompetenceAll.Items.Add(Item);
                    try
                    {
                        if (LoadCompetence[i].competences != null)
                        {
                            for (int j = 0; j < LoadCompetence[i].competences.Count; j++)

                            {
                                SabItem = new TreeViewItem();
                                SabItem.Tag = "C" + i;
                                SabItem.Header = LoadCompetence[i].competences[j].СompetenceName;
                                Item.Items.Add(SabItem);
                            }
                        }
                    }
                    catch { }
                }
            }
            catch { }

            


        }
    }

}
