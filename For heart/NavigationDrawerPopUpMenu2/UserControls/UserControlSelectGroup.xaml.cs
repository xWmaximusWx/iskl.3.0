﻿using MongoDB.Driver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ИСКО.Models;

namespace ИСКО.UserControls
{
    /// <summary>
    /// Логика взаимодействия для UserControlSelectGroup.xaml
    /// </summary>
    public partial class UserControlSelectGroup : UserControl
    {
        User user;
        GroupAdmin group;
        public UserControlSelectGroup()
        {
            InitializeComponent();
        }

        public UserControlSelectGroup(User us)
        {
            InitializeComponent();
            user = us;
            var client = new MongoClient("mongodb+srv://user:zOv1pGKMz0DJ4LfE@cluster0-at0y4.azure.mongodb.net/<dbname>?retryWrites=true&w=majority");
            var db = client.GetDatabase("SystemCorporateEdcation");
            var collection = db.GetCollection<GroupAdmin>("Groups");

            if(collection.Find(gr => true).ToList().Count > 0)
            {
                var groups = collection.Find(gr => gr.Admin == user.Id).ToList();
                group = groups[0];
                foreach (var group in groups)
                {
                    Button btn = new Button
                    {
                        Content = group.Name,
                        Margin = new Thickness(20),
                        Height = 150,
                        Width = 150,
                    };
                    btn.Click += new RoutedEventHandler(Button_Click);
                    btn.ContextMenu = new ContextMenu();

                    Button del = new Button
                    {
                        Content = "Удалить"
                    };

                    del.Click += new RoutedEventHandler(Del_Click);

                    btn.ContextMenu.Items.Add(del);
                    EditGroupButtons.Children.Add(btn);
                }
            }
            }

        private void Del_Click(object sender, RoutedEventArgs e)
        {
            var client = new MongoClient("mongodb+srv://user:zOv1pGKMz0DJ4LfE@cluster0-at0y4.azure.mongodb.net/<dbname>?retryWrites=true&w=majority");
            var db = client.GetDatabase("SystemCorporateEdcation");
            var collection = db.GetCollection<GroupAdmin>("Groups");

            collection.DeleteOne(groupa => groupa.Id == group.Id);
            EditGroupButtons.Children.Remove(EditGroupButtons.Children[1]);

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                if ((string)(sender as Button).Content == group.Name)
                {
                    MainGridSelectedGroup.Children.Clear();
                    MainGridSelectedGroup.Children.Add(new UserControlGroupEditing(user));
                }
            }
            catch
            {
                MainGridSelectedGroup.Children.Clear();
                MainGridSelectedGroup.Children.Add(new UserControlGroupEditing(user));
            }
        }
    }
}
