﻿using MaterialDesignThemes.Wpf;
using MongoDB.Driver;
using NavigationDrawerPopUpMenu2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ИСКО.Models;
using ИСКО.Windows;

namespace ИСКО.UserControls
{
    /// <summary>
    /// Логика взаимодействия для UserControlGroupEditing.xaml
    /// </summary>
    public partial class UserControlGroupEditing : UserControl
    {
        TreeViewItem root;
        List<User> users;
        IMongoCollection<User> collection;
        IMongoCollection<IndividualPlan> indivCollection;
        IMongoDatabase db;
        User ActiveUser;
        IndividualPlan ActivePlan;
        bool edit;
        public UserControlGroupEditing(User admin)
        {
            InitializeComponent();

            var client = new MongoClient("mongodb+srv://user:zOv1pGKMz0DJ4LfE@cluster0-at0y4.azure.mongodb.net/<dbname>?retryWrites=true&w=majority");
            db = client.GetDatabase("SystemCorporateEdcation");
            collection = db.GetCollection<User>("Users");
            indivCollection = db.GetCollection<IndividualPlan>("IndividualPlans");
            users = collection.Find(user => user.Status != true).ToList();
            SortedSet<string> Dep = new SortedSet<string>();
            SortedSet<string> Pos = new SortedSet<string>();
            foreach (var us in users)
            {
                Dep.Add(us.Department);
                Pos.Add(us.Position);
                
            }
            edit = false;
            var tree = new TreeViewItem();

            tree.Header = "ОТДЕЛЫ";
            tree.Tag = "Отделы";
            tree.Items.Add(null);

            tree.Expanded += ExpandedDepandens;

            PersonalDirectly.Items.Add(tree);

            tree = new TreeViewItem();
            tree.Header = "ДОЛЖНОСТИ";
            tree.Tag = "Должности";
            tree.Items.Add(null);
            tree.Expanded += ExpandedDepandens;
            PersonalDirectly.Items.Add(tree);


        }



        #region 
        /// <summary>
        /// Work with Tree
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExpandedDepandens(object sender, RoutedEventArgs e)
        {
            var item = (TreeViewItem)sender;

            if (item.Items.Count != 1 || item.Items[0] != null)
                return;
            item.Items.Clear();
            SortedSet<string> sSet = new SortedSet<string>();

            if((string)item.Tag == "Отделы") 
            users.ForEach(us =>
                sSet.Add(us.Department));
            else users.ForEach(us =>
                 sSet.Add(us.Position));

            sSet.ToList().ForEach(it =>
                {
                    var subItem = new TreeViewItem
                    {
                        Header = it,
                        Tag = it
                    };

                    subItem.Items.Add(null);
                    subItem.Expanded += ExpandedDepUsers;

                    item.Items.Add(subItem);
                }
                
            );

        }

        private void ExpandedDepUsers(object sender, RoutedEventArgs e)
        {
            var item = (TreeViewItem)sender;
            root = item;
            //if (item.Items.Count != 1 || item.Items[0] != null)
            //    return;


            item.Items.Clear();
            List<User> list = new List<User>();
            foreach(var us in users)
            {
                if(us.Department == (string)item.Tag || us.Position == (string)item.Tag)
                    list.Add(us);
            }
            list.ForEach(it =>
            {
                var subItem = new TreeViewItem
                {
                    Header = it.LName + " " + it.FName[0] + ". " + it.MName[0] + '.',
                    Tag = it.LName + " " + it.FName[0] + ". " + it.MName[0] + '.'
                };

                subItem.Selected += UserActiviti;
                subItem.ContextMenu = new ContextMenu();
                MenuItem menuItem = new MenuItem
                {
                    Header = "Удалить",
                    Tag = subItem.Tag,
                    
                };
                menuItem.Click += MenuItem_Click;
                subItem.ContextMenu.Items.Add(menuItem);

                //subItem.Items.Add(null);

                item.Items.Add(subItem);
            });
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            string head = null;
            try
            {
                head = (string)(sender as MenuItem).Tag;
                
            } catch { }
            if (head != null)
            {
                User sel = users.Find(us => (us.LName + " " + us.FName[0] + ". " + us.MName[0] + '.') == head);
                collection.DeleteOne(us => sel.Id == us.Id);
                users.Remove(sel);
                root.IsExpanded = false;
                root.Items.Clear();
                root.Items.Add(null);
                root.IsExpanded = true;
            }

        }


        #endregion



        //public UserControlGroupEditing(User admin, GroupAdmin groupAdmin)
        //{
        //    InitializeComponent();

        //    var client = new MongoClient("mongodb+srv://user:zOv1pGKMz0DJ4LfE@cluster0-at0y4.azure.mongodb.net/<dbname>?retryWrites=true&w=majority");
        //    db = client.GetDatabase("SystemCorporateEdcation");
        //    collection = db.GetCollection<User>("Users");
        //    users = collection.Find(user => user.Status != true).ToList();
        //    group = groupAdmin;
        //    editGroup = true;
        //    foreach (var us in users)
        //    {
        //        bool flag = false;
        //        foreach(var l in group.ListUsers)
        //        {
        //            if(us == l)
        //            {
        //                flag = true;
        //                var element = new ListViewItem();
        //                element.PreviewMouseLeftButtonUp += new MouseButtonEventHandler(UserActiviti);
        //                TextBlock textBlock = new TextBlock
        //                {
        //                    Text = us.LName + " " + us.FName[0] + ". " + us.MName[0] + '.'
        //                };
        //                PackIcon icon = new PackIcon
        //                {
        //                    Kind = PackIconKind.Delete,
        //                    BorderBrush = null,
        //                    Width = 20,
        //                    Height = 20,

        //                };
        //                Button btn = new Button
        //                {
        //                    Content = icon,
        //                    Background = null,
        //                    BorderBrush = null,
        //                    Foreground = Brushes.Gray,
        //                    Margin = new Thickness(0, -5, 8, 0),
        //                    HorizontalAlignment = HorizontalAlignment.Right,
        //                };

        //                btn.Click += new RoutedEventHandler(AddUserInGroup);

        //                Grid PanelElemint = new Grid
        //                {
        //                    Width = 182
        //                };

        //                PanelElemint.ColumnDefinitions.Add(new ColumnDefinition());
        //                PanelElemint.ColumnDefinitions.Add(new ColumnDefinition());
        //                Grid.SetColumn(textBlock, 0);
        //                Grid.SetColumn(btn, 1);

        //                PanelElemint.Children.Add(textBlock);
        //                PanelElemint.Children.Add(btn);

        //                element.Content = PanelElemint;
        //                Personals.Items.Add(element);
        //                break;
        //            }

        //        }

        //        if (!flag)
        //        {
        //            var element = new ListViewItem();
        //            element.PreviewMouseLeftButtonUp += new MouseButtonEventHandler(UserActiviti);
        //            TextBlock textBlock = new TextBlock
        //            {
        //                Text = us.LName + " " + us.FName[0] + ". " + us.MName[0] + '.'
        //            };
        //            PackIcon icon = new PackIcon
        //            {
        //                Kind = PackIconKind.Plus,
        //                BorderBrush = null,
        //                Width = 20,
        //                Height = 20,

        //            };
        //            Button btn = new Button
        //            {
        //                Content = icon,
        //                Background = null,
        //                BorderBrush = null,
        //                Foreground = Brushes.Gray,
        //                Margin = new Thickness(0, -5, 8, 0),
        //                HorizontalAlignment = HorizontalAlignment.Right,
        //            };

        //            btn.Click += new RoutedEventHandler(AddUserInGroup);

        //            Grid PanelElemint = new Grid
        //            {
        //                Width = 182
        //            };

        //            PanelElemint.ColumnDefinitions.Add(new ColumnDefinition());
        //            PanelElemint.ColumnDefinitions.Add(new ColumnDefinition());
        //            Grid.SetColumn(textBlock, 0);
        //            Grid.SetColumn(btn, 1);

        //            PanelElemint.Children.Add(textBlock);
        //            PanelElemint.Children.Add(btn);

        //            element.Content = PanelElemint;
        //            Personals.Items.Add(element);
        //        } 
        //        isEdit = false;

        //    }



        //    //GroupName.Text = groupAdmin.Name;
        //}

        private void UserActiviti(object sender, RoutedEventArgs e)
        {
            string a = (sender as TreeViewItem).Header.ToString();

            if(a != null)
            {
                User sel = users.Find(us => (us.LName + " " + us.FName[0] + ". " + us.MName[0] + '.') == a);
                sel.updateFIO();
                ActiveUser = sel;
                ActivePlan = null;
                AboutUser.Children.Clear();
                AboutUser.Children.Add(new UserControlAboutUser(sel));

                if(indivCollection.Find(ic => ic.UserId == sel.Id).ToList().Count == 1)
                {
                    ActivePlan = indivCollection.Find(ic => ic.UserId == sel.Id).ToList()[0];
                    PlanEducation.Content = new StackPanel
                    {
                        Orientation = Orientation.Horizontal,

                    };
                    (PlanEducation.Content as StackPanel).Children.Add(new PackIcon
                    {
                        Kind = PackIconKind.Edit,
                        Margin = new Thickness(0, 2, 0, 0)
                    });
                    (PlanEducation.Content as StackPanel).Children.Add(new TextBlock
                    {
                        Text = "Редактировать план обучения"
                    });
                    edit = true;
                } else
                {
                    edit = false;
                    PlanEducation.Content = new StackPanel
                    {
                        Orientation = Orientation.Horizontal,

                    };
                    (PlanEducation.Content as StackPanel).Children.Add(new PackIcon
                    {
                        Kind = PackIconKind.Done,
                        Margin = new Thickness(0, 2, 0, 0)
                    });
                    (PlanEducation.Content as StackPanel).Children.Add(new TextBlock
                    {
                        Text = "Назначить план обучения"
                    });
                }
            }
        }

        //private void AddUserInGroup(object sender, RoutedEventArgs e)
        //{
        //    string a = (((sender as Button).Parent as Grid).Children[0] as TextBlock).Text;

        //    if (a != null && ((sender as Button).Content as PackIcon).Kind == PackIconKind.Plus)
        //    {
        //        User sel = users.Find(us => (us.LName + " " + us.FName[0] + ". " + us.MName[0] + '.') == a);
        //        group.ListUsers.Add(sel);
        //        ActiveUser = sel;
        //        (sender as Button).Content = new PackIcon
        //        {
        //            Kind = PackIconKind.Delete
        //        };
        //    } else
        //    {
        //        User sel = users.Find(us => (us.LName + " " + us.FName[0] + ". " + us.MName[0] + '.') == a);
        //        group.ListUsers.Remove(sel);
        //        (sender as Button).Content = new PackIcon
        //        {
        //            Kind = PackIconKind.Plus
        //        };
        //    }
        //}

        private void CreateEmployee(object sender, RoutedEventArgs e)
        {
            var page = new WindowCreateEmployee();
            page.Show();
            page.Closed += Page_Closed;
        }

        private void Page_Closed(object sender, EventArgs e)
        {
            users = collection.Find(user => user.Status != true).ToList();
        }

        private void CreateNewLearningPlanBtn(object sender, RoutedEventArgs e)
        {

            try
            {
                WindowCreateNewLearningPlain pageCreate = null;
                if (edit)
                {
                    pageCreate = new WindowCreateNewLearningPlain(ActiveUser, ActivePlan);
                }
                else
                {
                    pageCreate = new WindowCreateNewLearningPlain(ActiveUser);
                }
                pageCreate.Show();
            } catch { }
            
        }

    }
}
