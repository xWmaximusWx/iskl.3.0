﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using CefSharp;
//using CefSharp.Wpf;
using System.IO;
using ИСКО.UserControls;
using ИСКО.Models;
using MongoDB.Driver;

namespace ИСКО
{
    /// <summary>
    /// Логика взаимодействия для UserControlLearning.xaml
    /// </summary>
    public partial class UserControlLearning : UserControl
    {
        public UserControlLearning(User user)
        {
            InitializeComponent();


            var client = new MongoClient("mongodb+srv://user:zOv1pGKMz0DJ4LfE@cluster0-at0y4.azure.mongodb.net/<dbname>?retryWrites=true&w=majority");
            var db = client.GetDatabase("SystemCorporateEdcation");
            var collection = db.GetCollection<IndividualPlan>("IndividualPlans");

            IndividualPlan plan = null;
            if (collection.Find(pl => pl.UserId == user.Id).ToList().Count > 0)
            {
                plan = collection.Find(pl => pl.UserId == user.Id).ToList()[0];
            }

            if(plan != null)
            {
                var wind = new StackPanel
                {
                    Width = 1010,
                    Orientation = Orientation.Horizontal,
                    Margin = new Thickness(50),
                };


                foreach (var act in plan.activities)
                {
                    var subItem = new StackPanel
                    {
                        Orientation = Orientation.Vertical,
                        Margin = new Thickness(50),
                        Width = 190,
                        Height = 210,
                        
                    };

                    subItem.Children.Add(new TextBlock
                    {
                        Text = act.Name,
                        TextAlignment = TextAlignment.Center,
                        Foreground = Brushes.Black,
                        FontSize = 18,
                        Height = 100,
                        Background = Brushes.LightGray

                    });

                    subItem.Children.Add(new Button
                    {
                        Content = "Продолжить обучение",
                        Height = 40,
                        Width = 190,
                    });

                    wind.Children.Add(subItem);
                }

                Browser.Children.Add(wind);
            } else
            {
                Browser.Children.Add(new TextBlock
                {
                    Text = "Нет назначенных курсов",
                    Foreground = Brushes.Black,
                });
            }

            
        }

    }
}
