﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ИСКО.Models;

namespace ИСКО
{
    /// <summary>
    /// Логика взаимодействия для UserControlChat.xaml
    /// </summary>
    public partial class UserControlChat : UserControl
    {
        MongoClient client;
        List<string> users;
        User _user;
        private IMongoCollection<Chat> chat;
        Chat chatSourse;

        public UserControlChat(User user)
        {
            InitializeComponent();
            _user = user;
            client = new MongoClient("mongodb+srv://user:zOv1pGKMz0DJ4LfE@cluster0-at0y4.azure.mongodb.net/<dbname>?retryWrites=true&w=majority");
            var db = client.GetDatabase("SystemCorporateEdcation");
            var collection = db.GetCollection<User>("Users");
            
            users = new List<string>();
            collection.Find(us => true).ToList().ForEach(it => users.Add(it.LName + " " + it.FName[0] + ". " + it.MName[0] + '.'));
            ListUsers.ItemsSource = users;

            chat = db.GetCollection<Chat>("Chat");
            
            chatSourse = chat.Find(ch => true).ToList()[0];
            if(chatSourse.Messages == null)
            {
                chatSourse.Messages = new List<Message>();
                chat.ReplaceOne(o => o.Id == chatSourse.Id, chatSourse);
            }


            foreach(var mes in chatSourse.Messages)
            {
                var sender = collection.Find(us => us.Id == mes.Sender).ToList()[0];

                var li = new ListViewItem
                {
                    HorizontalAlignment = HorizontalAlignment.Center
                };


                var st = new StackPanel
                {
                    Orientation = Orientation.Horizontal,
                    Width = 780,
                };

                st.Children.Add(new TextBlock
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    Margin = new Thickness(10, 0, 40, 0),
                    Text = mes.TimeSend.ToString("hh:mm:ss"),

                });

                st.Children.Add(new TextBlock
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    Margin = new Thickness(10, 0, 40, 0),
                    Text = sender.LName + " " + sender.FName[0] + ". " + sender.MName[0] + '.',
                    Width = 100
                });

                st.Children.Add(new TextBlock
                {
                    Text = mes.TextMessage,
                    Width = 540
                });

                li.Content = st;

                ChatMessage.Items.Add(li);
            }


        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DateTime time = DateTime.Now;
            Message mes = new Message
            {
                TextMessage = MessageTextBox.Text,
                TimeSend = time,
                Sender = _user.Id
            };
            chatSourse.Messages.Add(mes);
            chat.ReplaceOne(ch => ch.Id == chatSourse.Id, chatSourse);

            var li = new ListViewItem
            {
                HorizontalAlignment = HorizontalAlignment.Center
            };


            var st = new StackPanel
            {
                Orientation = Orientation.Horizontal,
                Width = 780,
            };

            st.Children.Add(new TextBlock
            {
                HorizontalAlignment = HorizontalAlignment.Left,
                Margin = new Thickness(10, 0, 40, 0),
                Text = mes.TimeSend.ToString("hh:mm:ss"),

            });

            st.Children.Add(new TextBlock
            {
                HorizontalAlignment = HorizontalAlignment.Left,
                Margin = new Thickness(10, 0, 40, 0),
                Text = _user.LName + " " + _user.FName[0] + ". " + _user.MName[0] + '.',
                Width = 100
            });

            st.Children.Add(new TextBlock
            {
                Text = mes.TextMessage,
                Width = 540
            });

            li.Content = st;

            ChatMessage.Items.Add(li);

            MessageTextBox.Text = "";
        }

        
    }
}
