﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ИСКО
{
    public class User
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string MName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public bool Status { get; set; }
        public string FIO { get; set; }
        public string Imgage { get; set; }
        public string Work { get; set; }
        public string Group { get; set; }
        public string Department { get; set; } // отдел
        public string Position { get; set; }  // должность

        public User()
        {

        }

        public string getFIO()
        {
            return string.Format("{0} {1} {2}", LName.ToUpperInvariant(), FName.ToUpperInvariant(), MName.ToUpperInvariant());
        }

        public void updateFIO()
        {
            FIO = LName + " " + FName + " " + MName;
        }

    }
}
