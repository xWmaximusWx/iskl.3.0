﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;







namespace ИСКО.Models
{
    public class Message
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public DateTime TimeSend { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string Sender { get; set; }
        public string TextMessage { get; set; }
    }
}