﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ИСКО.Models
{
    public class Activity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Name { get; set; }
        public string MethodEducation { get; set; }
        public float DataCloc { get; set; }
        public DateTime StartActivity { get; set; }
        public DateTime StopActivity { get; set; }
        public string Characteristic { get; set; }
    }
}
