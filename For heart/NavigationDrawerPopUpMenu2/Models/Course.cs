﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ИСКО.Models
{
    public class Course
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string NameOfCourse { get; set; }
        public string GroupOfCourse { get; set; }
        public string Description { get; set; }
        public float ValueOfCourse { get; set; }
        public string TypeOfDocument { get; set; }
        public string MethodOfEducation { get; set; }
        public List<KeyValuePair<string, string>> CompId { get; set; }
        public string Link { get; set; }
    }
}
