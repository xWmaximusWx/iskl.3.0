﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ИСКО.Models;

namespace ИСКО
{

    public class Competence
    {
        public Competence()
        {

        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string id { get; set; }
        public string MetaName { get; set; }
        public string СompetenceName { get; set; }
        public string Definition { get; set; }
        public List<WorkScalBall> workScalBall { get; set; }
    }
    public class WorkScalBall
    {
        public WorkScalBall()
        {
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string id { get; set; }
        public string Position { get; set; }
        public string scaling { get; set; }
        public string Balss { get; set; }
    }

    public class MetaCompetence
    {
        public MetaCompetence()
        {

        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string id { get; set; }
        public string MetaСompetenceName { get; set; }
        public string MetaDefinition { get; set; }
        public List<Competence> competences { get; set; }

    }

}