﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ИСКО.Models
{
    public class IndividualPlan
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }
        public DateTime PeriodStart { get; set; }
        public DateTime PeriodStop { get; set; }
        public string Recomendation { get; set; }
        public List<Activity> activities { get; set; }
        public string Komments { get; set; }
        public string Responsible { get; set; }

    }
}
