﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ИСКО.Models
{
    public class GroupAdmin
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string Admin { get; set; }
        public string Name { get; set; }
        public string MethodsEdu { get; set; }
        public List<User> ListUsers { get; set; }
        public List<Course> ListCourses { get; set; }


        public GroupAdmin()
        {
            ListUsers = new List<User>();
            ListCourses = new List<Course>();
        }
    }
}
